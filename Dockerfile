FROM python:3
WORKDIR /opt

# ==> Add Dagster layer
RUN \
       apt-get update -yqq \
    && apt-get install -yqq cron



# ==> Add user code layer
# install requirements
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY dagster_pipeline dagster_pipeline
