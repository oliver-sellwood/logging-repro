import dagster
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

##### define pipeline solids #####


@dagster.solid(
    input_defs=[
        dagster.InputDefinition(name="data", dagster_type=int),
    ],
    output_defs=[
        dagster.OutputDefinition(name="data", dagster_type=int)
    ],
)
def log_debug(context, data):

    context.log.debug("this is debug", value=data)
    logger.debug("its a debug")

    return data + 1


@dagster.solid(
    input_defs=[
        dagster.InputDefinition(name="data", dagster_type=int),
    ],
    output_defs=[
        dagster.OutputDefinition(name="data", dagster_type=int)
    ],
)
def log_info(context, data):

    context.log.info("this is info", value=data)
    logger.info("its a info")

    return data + 1


@dagster.solid(
    input_defs=[
        dagster.InputDefinition(name="data", dagster_type=int),
    ],
    output_defs=[
        dagster.OutputDefinition(name="data", dagster_type=int)
    ],
)
def log_warning(context, data):

    context.log.warning("this is warning", value=data)
    logger.warning("its a warning")

    return data + 1


@dagster.solid(
    input_defs=[
        dagster.InputDefinition(name="data", dagster_type=int),
    ],
    output_defs=[
        dagster.OutputDefinition(name="data", dagster_type=int)
    ],
)
def log_error(context, data):

    context.log.error("this is error", value=data)
    logger.error("its a error")

    return data + 1


@dagster.solid(
    input_defs=[
        dagster.InputDefinition(name="data", dagster_type=int),
    ],
    output_defs=[
        dagster.OutputDefinition(name="data", dagster_type=int)
    ],
)
def log_critical(context, data):

    context.log.critical("this is critical", value=data)
    logger.error("its a critical")


    return data + 1


#### Define the pipeline ####
loggers = {
    "json": dagster.loggers.json_console_logger
}

modes = [
    dagster.ModeDefinition(
        logger_defs=loggers
    )
]


@dagster.pipeline(
    mode_defs = modes
)
def count():

    x = log_debug()
    x = log_info(x)
    x = log_warning(x)
    x = log_error(x)
    x = log_critical(x)

@dagster.repository
def data_platform():


    return {
        "pipelines": {
            "count": lambda: count,        
        },

    }
