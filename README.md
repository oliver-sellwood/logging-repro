# Dagster missing logs error

To repro

1. 
```
docker build . -t min_repro
docker run --rm --name min_repoo -p 3000:3000 min_repro dagit -f dagster_pipeline/main.py -h 0.0.0.0
```

2. load up dagit and use the following configs

```
solids:
  log_debug:
    inputs:
      data: 0
loggers:
  json:
    config:
      log_level: DEBUG
```

```
solids:
  log_debug:
    inputs:
      data: 0
loggers:
  json:
    config:
      log_level: INFO
```

```
solids:
  log_debug:
    inputs:
      data: 0
loggers:
  json:
    config:
      log_level: ERROR
```


3. Observer docker logs and see
    - missing user generated logging
    - debug level logs in info level logs
    - even the regular python logs do not show up depsite appropriate setup

I have also included my outputs in `level_debug.log` and `level_info.log`